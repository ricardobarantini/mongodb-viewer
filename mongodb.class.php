<?php

class MongoDB {
    private $host = '165.227.20.36';
    private $user = 'cic';
    private $password = 'SMb3O6w9eG4h8Zd08ta1356x1VV9Bt2120zd97x9bs64t4sd';
    private $database = 'deeddata';
    private $port = 27017;

    /**
     * [connection description]
     * @return [type] [description]
     */
    public function connection() {
        return new MongoDB\Driver\Manager('mongodb://' . $this->user . ':' . $this->password . '@' . $this->host . ':' . $this->port . '/' . $this->database);
    }

    /**
     * [select description]
     * @param  [type] $collection [description]
     * @param  array  $where      [description]
     * @param  array  $options    [description]
     * @return [type]             [description]
     */
    public function select($collection, $where = array(), $options = array()) {
        $conn = $this->connection();
        $query = new MongoDB\Driver\Query($where, $options);
        return $conn->executeQuery($this->database . '.' . $collection, $query);
    }

    /**
     * [dropdown description]
     * @param  [type] $collection [description]
     * @param  [type] $key        [description]
     * @param  [type] $value      [description]
     * @return [type]             [description]
     */
    public function dropdown($collection, $key, $value = null) {
        if ($key == $value || is_null($value)) {
            $options = array('projection' => array($key => 1), 'sort' => array($key => 1));
        } else {
            $options = array('projection' => array($key => 1, $value => 1), 'sort' => array($value => 1));
        }
        return $this->select($collection, array(), $options);
    }

    /**
     * [counties description]
     * @param  [type] $state [description]
     * @return [type]        [description]
     */
    public function counties($filter) {
        $result = array();
        $result['data'] = array();
        $result['count'] = 0;
        $query = $this->select('vendor_yodatasource_counties',
                               array('county' => new MongoDB\BSON\Regex('^(?i)'.$filter)),
                               array(
                                   'projection' => array('state' => 1, 'county' => 1),
                                   'sort' => array('state' => 1, 'county' => 1)
                               )
                              );
        foreach ($query as $q) {
            $result['count'] = $result['count']+1;
            if($result['count'] <= 50){  //only 50 results back
                array_push($result['data'], array('id' => $q->state.'|||'.$q->county, 'text' => $q->state.' - '.$q->county));
            }
        }
        return json_encode($result);
    }

    /**
     * [filter description]
     * @param  array    $counties [description]
     * @param  int|null $limit    [description]
     * @return [type]             [description]
     */
    public function filter(array $counties, $sic, $limit) {
        $records = array();
        $records['data'] = array();
        $records['total'] = 0;
        $count = 0;
        $filter = array();
        $filter['$and'] = array();
        if($sic){
            array_push($filter['$and'], array('SIC' =>  new MongoDB\BSON\Regex('^'.$sic)));
        }
        if($counties && $counties[0] != ""){
            $or = array();
            foreach ($counties as &$record) {
                $data = explode("|||", $record);
                array_push($or, array('State' => $data[0], 'County' => $data[1]));
            }
            array_push($filter['$and'], array('$or' =>  $or));
        }
        //echo '<pre>';
        //echo json_encode($filter, JSON_PRETTY_PRINT);
        //echo '</pre>';
        $query = $this->select('vendor_yodatasource', $filter);
        foreach ($query as $q) {
            $records['total']++;
            if ($records['total'] <= $limit) {
                array_push($records['data'], array(
                    'state' => isset($q->State) ? $q->State : '',
                    'county' => isset($q->County) ? $q->County : '',
                    'city' => isset($q->City) ? $q->City : '',
                    'sic' => isset($q->SIC) ? $q->SIC : '',
                    'name' => isset($q->Name) ? $q->Name : '',
                    'address' => isset($q->Address) ? $q->Address : '',
                    'phone' => isset($q->Phone) ? $q->Phone : '',
                    'fax' => isset($q->Fax) ? $q->Fax : '',
                    'tollfreephone' => isset($q->TollFreePhone) ? $q->TollFreePhone : '',
                    'contactname' => isset($q->ContactName) ? $q->ContactName : ''
                ));
            }
        }
        return json_encode($records);
    }


    public function csv(array $counties, $sic, $filename) {
        $filter = array();
        $filter['$and'] = array();
        if($sic){
            array_push($filter['$and'], array('SIC' =>  new MongoDB\BSON\Regex('^'.$sic)));
        }
        if($counties && $counties[0] != ""){
            $or = array();
            foreach ($counties as &$record) {
                $data = explode("|||", $record);
                array_push($or, array('State' => $data[0], 'County' => $data[1]));
            }
            array_push($filter['$and'], array('$or' =>  $or));
        }
        $query = $this->select('vendor_yodatasource', $filter);
        $fp = fopen('../csv/' . $filename . '.csv', 'w');
        $count = 0;
        foreach ($query as $line) {
            if($count == 0){
                 fputcsv($fp, array_keys(get_object_vars($line)), ",");
                 $count = 1;
            }
            fputcsv($fp, get_object_vars($line), ",");
        }
        fclose($fp);
    }
}
