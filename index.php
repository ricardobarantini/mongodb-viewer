<?php
ini_set('display_errors', 1);
include('mongodb.class.php');
$mongo = new MongoDB;

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>MongoDB Viewer</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css">
    <link rel="stylesheet" href="css/app.css">
</head>
<body>
    <div class="container">
        <div class="row">
            <form action="#">
                <div class="col-md-5">
                    <select name="sic" id="sic" class="form-control">
                        <?php foreach($mongo->dropdown('sic_codes_4digits', 'code', 'description') as $item): ?>
                        <option value="<?php echo $item->code; ?>"><?php echo $item->description.' - '.$item->code; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>

                <div class="col-md-5">
                    <select name="counties" id="counties" class="form-control" multiple="multiple" placeholder="Counties"></select>
                </div>

                <div class="col-md-1">
                    <button class="btn btn-primary btn-block" id="filter">Filter</button>
                </div>

                <div class="col-md-1">
                    <button class="btn btn-success btn-block" id="csv">CSV</button>
                </div>
            </form>

            <div class="col-md-12">
                <div class="alert alert-info hidden" id="loading">Loading records...</div>
                <div class="alert alert-info hidden" id="generating"><strong>Wait.</strong> Generating csv file...</div>
                <div class="alert alert-success hidden" id="download"><strong>Done!</strong> <a href="#">Click here to download the file.</a></div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Data sample <span id="records-count"></span>
                    </div>

                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>State</th>
                                    <th>County</th>
                                    <th>City</th>
                                    <th>SIC</th>
                                    <th>Name</th>
                                    <th>Address</th>
                                    <th>Phone</th>
                                    <th>Fax</th>
                                    <th>TollFreePhone</th>
                                    <th>Contact Name</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr id="default-message">
                                    <td colspan="10">Use the above options to filter</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.full.min.js"></script>
    <script src="scripts/app.js"></script>
</body>
</html>
