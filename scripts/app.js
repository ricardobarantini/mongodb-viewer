$(document).ready(function() {
    $("#counties").select2({
          ajax: {
            url: "api/getCounties.php",
            type: 'POST',
            dataType: 'json',
            delay: 1000,
            data: function (params) {
              return {
                q: params.term, // search term
                //field: this.data.upstream_name,
                page: params.page
              };
            },
            processResults: function (result, params) {
              //console.log('init');
              // parse the results into the format expected by Select2
              // since we are using custom formatting functions we do not need to
              // alter the remote JSON data, except to indicate that infinite
              // scrolling can be used
              params.page = params.page || 1;

              return {
                results: result.data,
                pagination: {
                  more: (params.page * 30) < result.count
                }
              };
            },
            cache: true
          },
          multipleselect: true,
          multiple: true,
          //theme: "bootstrap",
          escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
          minimumInputLength: 2,
          templateResult: function (repo) {
            var markup = '<div class="clearfix">' +
            '<div class="col-sm-12">' + repo.text + '</div></div>';
            return markup;
          },
          templateSelection: function (repo) {
            return repo.text;
          }
    });

    $('#filter').on('click', function(e) {
        e.preventDefault();
        $('#loading').removeClass('hidden');
        $('#default-message').addClass('hidden');
        var counties = $('#counties').val();
        var sic = $('#sic').val();
        $.ajax({
            url: 'api/filter.php',
            data: { county: counties, sic: sic},
            type: 'POST',
            dataType: 'json',
            success: function() {
                //$('#loading').addClass('hidden');
            }
        }).done(function(result) {
            $('table.table tbody').html('');
            $.each(result.data, function(key, value) {
                $('table.table tbody').append('\
                    <tr>\
                        <td>' + value.state + '</td>\
                        <td>' + value.county + '</td>\
                        <td>' + value.city + '</td>\
                        <td>' + value.sic + '</td>\
                        <td>' + value.name + '</td>\
                        <td>' + value.address + '</td>\
                        <td>' + value.phone + '</td>\
                        <td>' + value.fax + '</td>\
                        <td>' + value.tollfreephone + '</td>\
                        <td>' + value.contactname + '</td>\
                    </tr>');
            });
            $('#records-count').text(result.total);
            $('#loading').addClass('hidden');
        });
    });

    $('#csv').on('click', function(e) {
        e.preventDefault();
        $('#generating').removeClass('hidden');
        var counties = $('#counties').val();
        var sic = $('#sic').val();
        var state = $('#state').val();
        var filename = new Date().getTime();
        $.ajax({
            url: 'api/csv.php',
            data: { county: counties, sic: sic, state: state, filename: filename },
            type: 'POST',
            success: function() {
                $('#generating').addClass('hidden');
            }
        }).done(function() {
            document.location.href="api/download.php?filename=" + filename + ".csv";
        });
    });
});
